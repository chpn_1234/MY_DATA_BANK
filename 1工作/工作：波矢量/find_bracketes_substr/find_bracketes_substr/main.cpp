#include<iostream>
#include<string>
#include<vector>

using namespace std;

int SORT_OUT(vector<string> &s_substrs){
	int n = s_substrs.size();
	if (n == 0) {
		cout << "不存在" << endl << endl;
		return 0;
	}
	for (int i = 0; i < n; ++i) {
		cout << s_substrs[i] << " " << endl;
	}
	cout << endl;
	return 0;
}

int FIND_BRACKETES_SUBSTR(string s, vector<string>& s_substrs) {
	string s_substr;
	int s_length = s.length();
	int left_index = 0;
	int max_length = 0;
	vector<string> temp_s_substrs;
	
	//----------找出有效字串------------
	for (int i = 0; i < s_length - 1; ++i) {
		if (s[i] != '(') {
			continue;
		}
		
		int leftBracketes = 1;

		for (int j = i + 1; j < s_length; ++j) {
			if (leftBracketes == 0 && s[j] != '(') {
				break;
			}

			if (s[j] == '(') {
				leftBracketes++;
				continue;
			}
			else if (s[j] == ')') {
				leftBracketes--;
			}
			

			if (leftBracketes == 0 && max_length <= j - i + 1) {
				max_length = j - i + 1;
				left_index  = i;
			}
		}
		if (max_length > 1&& left_index == i) {
			s_substr = s.substr(left_index, max_length);
			cout << "maybe:" << left_index <<" " << s_substr << endl;
			temp_s_substrs.push_back(s_substr);
		}
	}
	
	//--------找出最长字串--------
	int temp_s_substrs_size = temp_s_substrs.size();
	if (temp_s_substrs_size == 0) {
		return 0;
	}
	max_length = 0;
	for (int i = 0; i < temp_s_substrs_size; ++i) {
		if (max_length < temp_s_substrs[i].length()) {
			max_length = temp_s_substrs[i].length();
		}
	}
	for (int i = 0; i < temp_s_substrs_size; ++i) {
		if (max_length==temp_s_substrs[i].length()) {
			s_substrs.push_back(temp_s_substrs[i]);
		}
	}
	return 0;
}

int main() {
	string s_input;
	while (1) {
		cout << "输入字符串：" << endl;
		getline(cin, s_input);
		if (s_input == "s") {
			break;
		}

		//----------find substr--------
		vector<string> sInputSubstrs;
		FIND_BRACKETES_SUBSTR(s_input, sInputSubstrs);

		//---------输出子串------
		cout<<endl<< "目标子串：" << endl;
		SORT_OUT(sInputSubstrs);
	}

	return 0;
}